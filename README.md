# GIT workflow


## High level flow


![ALT](https://i0.wp.com/css-tricks.com/wp-content/uploads/2021/09/Untitled-2-1.png?resize=1000%2C219&ssl=1)


## Getting started

This document is a guideline for collaborating on this repository.



## Step 1 Clone files

```

git clone https://git.massiverocket.com/akshay.sudheer/python.git


```

## Step 2 Create a local branch

Once you have cloned your repo to your local. Create a branch in your local.

```
git checkout feature

```

## Step 3 Add to stage and commit


```
git add .
git commit -m "details of the changes"

```

## Step 4 Push to remote repo

```
git remote add origin https://git.massiverocket.com/akshay.sudheer/python.git
git push origin feature

```

## Step 5 Create a merge request 

In the gitlab you can navigate to your merge request and create a merge request. Someone will approve your merge. If there is a conflict you can resolve in the UI itself or you can make a pull request and resolve the changes in local and follow the same process from step 3

```
git pull --rebase origin develop

```

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Project status and version
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
